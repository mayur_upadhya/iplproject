// 1st Question
const matchPlayed = (matches) =>{
     let MatchesPlayed = {};
     for(let i=0; i<matches.length; i++){
         if(MatchesPlayed[matches[i].season]){
             MatchesPlayed[matches[i].season] += 1;
         }
         else {
             MatchesPlayed[matches[i].season] = 1;
         }
     }
     return MatchesPlayed;
}
// 2nd Question
const matchesWin = (matches) =>{
     const matchesWonPerTeam=(matches, year) => {
         let matchesTeamWon = {};
         for(let i=0; i<matches.length; i++){
             if(matches[i].season === year) {   
                 if(matchesTeamWon[matches[i].winner]){
                     matchesTeamWon[matches[i].winner]++;
                 }else{
                     matchesTeamWon[matches[i].winner] = 1;
                 }
             }
         }
         return matchesTeamWon;
     }
     let year = {};
     for(let i=0; i<matches.length; i++){
         if(!(year[matches[i].season])){
             year[matches[i].season] = matchesWonPerTeam(matches, matches[i].season);
         }
     }
     return year;
}
// 3rd Question
const extraRuns2016=(matches, deliveries) => {
     let extraRuns = {};
     for(let i=0; i<matches.length; i++){
         if(matches[i].season === '2016'){
             for(let j=0; j<deliveries.length; j++){
                 if(matches[i].id === deliveries[j].match_id){
                     if(extraRuns[deliveries[j].bowling_team]){
                         extraRuns[deliveries[j].bowling_team] += Number(deliveries[j].extra_runs);
                     } else {
                         extraRuns[deliveries[j].bowling_team] = Number(deliveries[j].extra_runs);
                     }
                 }
             }
         }
     }
     return extraRuns;
}
//4th Question
const topTenEconomicalBowlers = (matches, deliveries) => {
    let economicalBowlers = {};
    let overs = {};
    for(let i=0; i<matches.length; i++){
        if(matches[i].season ==='2015') {
             for(let j=0; j<deliveries.length; j++){
                 if(matches[i].id === deliveries[j].match_id){
                     if(economicalBowlers[deliveries[j].bowler]){
                         if(Number(deliveries[j].bye_runs) || Number(deliveries[j].legbye_runs)){
                             economicalBowlers[deliveries[j].bowler];
                         }else{
                             economicalBowlers[deliveries[j].bowler] += Number(deliveries[j].total_runs);
                         }
                         if(Number(deliveries[j].wide_runs) || Number(deliveries[j].noball_runs)){
                             overs[deliveries[j].bowler];
                         }else{
                             overs[deliveries[j].bowler]++;
                         }
                     }else {
                         economicalBowlers[deliveries[j].bowler] = Number(deliveries[j].total_runs);
                         if(Number(deliveries[j].wide_runs) || Number(deliveries[j].noball_runs)){
                             overs[deliveries[j].bowler] = 0;
                         }else{
                             overs[deliveries[j].bowler] = 1;
                        }
                         overs[deliveries[j].bowler] = 1;
                     }
                 }
             }
         }
     }
     for(let i in overs){
         economicalBowlers[i] = (economicalBowlers[i]/(overs[i]/6)).toFixed(2);
     }
     let economy = Object.entries(economicalBowlers)
     economy = economy.sort((a,b) => a[1]-b[1]).slice(0, 10).reduce((acc, curr) => {
         if(!acc[curr[0]]){
             acc[curr[0]] = curr[1];
         }
         return acc;
     }, {});
     return economy;
}
const ipl = (matches, deliveries) => {
    console.log(matchPlayed(matches));
    console.log(matchesWin(matches));  
    console.log(extraRuns2016(matches, deliveries));
    console.log(topTenEconomicalBowlers(matches, deliveries));
}
module.exports = ipl;